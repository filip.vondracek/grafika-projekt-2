package ukol.dva.enviroment.mapping;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import lvl1basic.p01start.p04utils.GridFactory;
import oglutils.*;
import transforms.*;

import java.awt.event.*;

public class Renderer implements GLEventListener, MouseListener, MouseMotionListener, KeyListener {

    int shaderProgram, locMat, locMat2;
    OGLTextureCube texture;
    Mat4 proj;
    Mat4 swapYZflipZ = new Mat4(new double[]{
            1, 0, 0, 0,
            0, 0, -1, 0,
            0, 1, 0, 0,
            0, 0, 1, 1,
    });

    private int width, height, ox, oy;

    private boolean Mat4PerspRHisOn = true;

    private OGLBuffers buffers, buffers2;
    private OGLTextRenderer textRenderer;

    private int shaderProgramViewer, locTransform, locMoveZ, locTime, locView, locProjection, locMode, locLightVP, locEyePosition, locLightPosition;

    private int locCube;

    private Mat4 projViewer;
    private Mat4 projLight = new Mat4OrthoRH(5, 5, 0.1, 20);
    private float time = 0;
    private float moveZ = 1.0f;

    private Camera camera = new Camera(), lightCamera;

    @Override
    public void init(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();
        OGLUtils.shaderCheck(gl);

        gl = OGLUtils.getDebugGL(gl);
        glDrawable.setGL(gl);

        OGLUtils.printOGLparameters(gl);

        textRenderer = new OGLTextRenderer(gl, glDrawable.getSurfaceWidth(), glDrawable.getSurfaceHeight());

        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
        gl.glEnable(GL2GL3.GL_DEPTH_TEST);

        shaderProgram = ShaderUtils.loadProgram(gl, "/ukol/dva/enviroment/mapping/map.vert",
                "/ukol/dva/enviroment/mapping/map.frag",
                null, null, null, null);


       shaderProgramViewer = ShaderUtils.loadProgram(gl, "/ukol/dva/enviroment/mapping/start");

        buffers = GridFactory.generateGrid(gl, 100, 100);

        lightCamera = new Camera()
                .withPosition(new Vec3D(1, 1, 5))
                .addAzimuth(5 / 4. * Math.PI)//-3/4.
                .addZenith(-1 / 5. * Math.PI);

        locTime = gl.glGetUniformLocation(shaderProgramViewer, "time");
        locMode = gl.glGetUniformLocation(shaderProgramViewer, "mode");
        locView = gl.glGetUniformLocation(shaderProgramViewer, "view");
        locMoveZ = gl.glGetUniformLocation(shaderProgramViewer, "moveZ");
        locTransform = gl.glGetUniformLocation(shaderProgramViewer, "rotace");
        locProjection = gl.glGetUniformLocation(shaderProgramViewer, "projection");
        locLightVP = gl.glGetUniformLocation(shaderProgramViewer, "lightVP");
        locEyePosition = gl.glGetUniformLocation(shaderProgramViewer, "eyePosition");
        locLightPosition = gl.glGetUniformLocation(shaderProgramViewer, "lightPosition");

        locCube = gl.glGetUniformLocation(shaderProgramViewer, "cubemap");

        locMat = gl.glGetUniformLocation(shaderProgram, "mat");
        locMat2 = gl.glGetUniformLocation(shaderProgramViewer, "mat");

        createBuffers(gl);

        String[] names = {"/textures/snow_positive_x.jpg",
                "/textures/snow_negative_x.jpg",
                "/textures/snow_negative_y.jpg",
                "/textures/snow_positive_y.jpg",
                "/textures/snow_positive_z.jpg",
                "/textures/snow_negative_z.jpg"};
        texture = new OGLTextureCube(gl, names);
        camera = camera.withPosition(new Vec3D(0.5, 0.5, 0.5))
                .addAzimuth(5 / 4. * Math.PI)
                .addZenith(-1 / 4. * Math.PI)
                .withFirstPerson(true)
                .withRadius(1);

        gl.glEnable(GL2GL3.GL_DEPTH_TEST);

    }


    @Override
    public void display(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();

        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        renderFromViewer(gl);

    }

    private void renderFromViewer(GL2GL3 gl) {
        gl.glUseProgram(shaderProgramViewer);

        gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0);
        gl.glViewport(0, 0, width, height);

        gl.glClearColor(0.0f, 0.3f, 0.0f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gl.glUniform1f(locTime, time);
        gl.glUniform1f(locMoveZ, moveZ);

        gl.glUniformMatrix4fv(locView, 1, false, camera.getViewMatrix().floatArray(), 0);
        gl.glUniformMatrix4fv(locProjection, 1, false, projViewer.floatArray(), 0);
        gl.glUniformMatrix4fv(locLightVP, 1, false, lightCamera.getViewMatrix().mul(projLight).floatArray(), 0);
        gl.glUniform3fv(locEyePosition, 1, ToFloatArray.convert(camera.getPosition()), 0);
        gl.glUniform3fv(locLightPosition, 1, ToFloatArray.convert(lightCamera.getPosition()), 0);

        gl.glUniformMatrix4fv(locMat2, 1, false,
                ToFloatArray.convert(swapYZflipZ.mul(camera.getViewMatrix()).mul(proj)), 0);
        texture.bind(shaderProgramViewer, "textureID", 0);

        buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgramViewer);

        gl.glUseProgram(shaderProgram);
        gl.glUniformMatrix4fv(locMat, 1, false,
                ToFloatArray.convert(swapYZflipZ.mul(camera.getViewMatrix()).mul(proj)), 0);
        texture.bind(shaderProgram, "textureID", 0);
        buffers2.draw(GL2GL3.GL_TRIANGLES, shaderProgram);
    }


    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        this.width = width;
        this.height = height;
        textRenderer.updateSize(width, height);

        setProjekce();
        proj = new Mat4PerspRH(Math.PI / 3, height / (double) width, 0.01, 1000.0);

    }

    private void setProjekce() {
        double ratio = height / (double) width;
        //projLight = new Mat4OrthoRH(5 / ratio, 5, 0.8, 40);
        if (Mat4PerspRHisOn) {
            System.out.println("persp");
            projViewer = new Mat4PerspRH(Math.PI / 3, ratio, 1, 20.0);
        } else {
            System.out.println("ortho");
            projViewer = new Mat4OrthoRH(5 / ratio, 5, 0.1, 20);
        }
    }

    @Override
    public void dispose(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();
        gl.glDeleteProgram(shaderProgramViewer);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ox = e.getX();
        oy = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        camera = camera.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
                .addZenith((double) Math.PI * (e.getY() - oy) / width);
        ox = e.getX();
        oy = e.getY();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        float speed = 0.1f;
        switch (e.getKeyCode()) {
            case KeyEvent.VK_A:
                camera = camera.addAzimuth(-speed);
                break;
            case KeyEvent.VK_D:
                camera = camera.addAzimuth(speed);
                break;
            case KeyEvent.VK_W:
                camera = camera.addZenith(speed);
                break;
            case KeyEvent.VK_S:
                camera = camera.addZenith(-speed);
                break;
            case KeyEvent.VK_C:
                this.Mat4PerspRHisOn = !Mat4PerspRHisOn;
                setProjekce();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }


    void createBuffers(GL2GL3 gl) {
        // vertices are not shared among triangles (and thus faces) so each face
        // can have a correct normal in all vertices
        // also because of this, the vertices can be directly drawn as GL_TRIANGLES
        // (three and three vertices form one face)
        // triangles defined in index buffer
        int s = 1; //size
        float[] cube = {
                // bottom (z-) face
                s, 0, 0, 0, 0, -s,
                0, 0, 0, 0, 0, -s,
                s, s, 0, 0, 0, -s,
                0, s, 0, 0, 0, -s,
                // top (z+) face
                s, 0, s, 0, 0, s,
                0, 0, s, 0, 0, s,
                s, s, s, 0, 0, s,
                0, s, s, 0, 0, s,
                // x+ face
                s, s, 0, s, 0, 0,
                s, 0, 0, s, 0, 0,
                s, s, s, s, 0, 0,
                s, 0, s, s, 0, 0,
                // x- face
                0, s, 0, -s, 0, 0,
                0, 0, 0, -s, 0, 0,
                0, s, s, -s, 0, 0,
                0, 0, s, -s, 0, 0,
                // y+ face
                s, s, 0, 0, s, 0,
                0, s, 0, 0, s, 0,
                s, s, s, 0, s, 0,
                0, s, s, 0, s, 0,
                // y- face
                s, 0, 0, 0, -s, 0,
                0, 0, 0, 0, -s, 0,
                s, 0, s, 0, -s, 0,
                0, 0, s, 0, -s, 0
        };

        int[] indexBufferData = new int[36];
        for (int i = 0; i < 6; i++) {
            indexBufferData[i * 6] = i * 4;
            indexBufferData[i * 6 + 1] = i * 4 + 1;
            indexBufferData[i * 6 + 2] = i * 4 + 2;
            indexBufferData[i * 6 + 3] = i * 4 + 1;
            indexBufferData[i * 6 + 4] = i * 4 + 2;
            indexBufferData[i * 6 + 5] = i * 4 + 3;
        }
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 3),
                new OGLBuffers.Attrib("inNormal", 3)
        };

        buffers2 = new OGLBuffers(gl, cube, attributes, indexBufferData);
    }
}