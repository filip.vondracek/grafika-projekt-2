#version 150
in vec3 pos_eye;
in vec3 n_eye;
uniform mat4 view;
uniform samplerCube textureID;
out vec4 frag_colour;

void main() {
	vec3 incident_eye = normalize(pos_eye);
	vec3 normal = normalize(n_eye);

	vec3 reflected = reflect(incident_eye, normal);
	reflected = vec3(inverse(view) * vec4(reflected, 0.0));

	frag_colour = texture(textureID, reflected);
}
