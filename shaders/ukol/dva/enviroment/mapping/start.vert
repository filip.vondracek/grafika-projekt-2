#version 150

in vec2 inPosition;
uniform mat4 projection;
uniform mat4 view;
uniform vec3 lightPosition;
uniform float time;
uniform int mode;
uniform mat4 lightVP;
uniform float moveZ;
uniform mat4 mat;
uniform mat4 rotace;
out vec3 pos_eye;
out vec3 n_eye;

const float PI = 3.14;

vec3 getSphere(vec2 xy) {
    float t = ((xy.x+1))/2 *PI;// 0 - PI
    float s = (xy.y+1) *PI;// 0 - 2PI

    //sfericke souradnice
    float rho = 1;
    float phi = t;
    float theta = s;

    //prevod sferickych souradnic na kartezske
    float x = rho*sin(theta)*cos(phi);
    float y = rho*sin(theta)*sin(phi);
    float z = rho*cos(theta);
    return vec3(x, y, z);
}

vec3 getSphereNormal(vec2 xy) {
    vec3 u = getSphere(xy + vec2(0.001, 0)) - getSphere(xy - vec2(0.001, 0));
    vec3 v = getSphere(xy + vec2(0, 0.001)) - getSphere(xy - vec2(0, 0.001));
    return cross(u, v);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 vp = getSphere(pos)/4.0;
    vec3 vn = getSphereNormal(pos);

    pos_eye = vec3(view * vec4(vp, 1.0));
    n_eye = vec3(view * vec4(vn, 0.0));
    gl_Position = projection * view * vec4(vp, 1.0);
}
